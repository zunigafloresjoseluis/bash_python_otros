#-*- coding:utf-8 -*- 


class Lampara:
    _LAMPS=['''
        .
   .    |   ,
    \   '  /
     `,--.'
---- (    ) ----
      \  /
     _|=|_
    |_____|
    ''',
    '''
       
      ,--.
     (    ) 
      \  /
     _|=|_
    |_____|
    ''']

    def __init__(self,prendido):
        self._prendido=prendido
    
    def encender(self):
        self._prendido=True
        self.mostrar_lampara()
    def apagar(self):
        self._prendido=False
        self.mostrar_lampara()
    
    def mostrar_lampara(self):
        if self._prendido==True:
            print(self._LAMPS[0])
        else:
            print(self._LAMPS[1])


def run():
    lamp=Lampara(prendido=True)
    while True:
        opcion=str(raw_input(''' 
            Lampara
            [p]render
            [a]pagar
            [s]alir

        '''))
        if opcion=='p':
            lamp.encender()
        if opcion=='a':
            lamp.apagar()
        if opcion=='s':
            break;

if __name__=='__main__':
    run()