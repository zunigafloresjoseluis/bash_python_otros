#-*- coding:utf-8 -*-


def contar_letra_palabra(_str):
    arreglo={}

    for i in range(len(_str)):
        contar=0
        for j in range(len(_str)):
            if _str[i]==_str[j]:
                contar+=1;
        arreglo[_str[i]]=contar
    return arreglo

def  unica_letra(diccionario):
    for key,value in diccionario.iteritems():
        if value==1:
            return key
    return '_'


if __name__=='__main__':
    print(unica_letra(contar_letra_palabra('wqeqweqwea')))
    

