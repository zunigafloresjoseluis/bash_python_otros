#-*- coding:utf-8 -*-
## reciben una funcio y  retornan  una mas 

def protected(func):
    def wrapper(password):
        if password=="jose":
            return func()
        else:
            print("no es  la contraseña")
    return wrapper

@protected
def protected_fun():
    print("tu  contraseña es correcta")

if __name__=='__main__':
    passw=str(raw_input("Ingrese la contraseña"))
    protected_fun(passw)
