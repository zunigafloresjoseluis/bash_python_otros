import React, { Component } from 'react';

import './App.css';
//utiliza className en vez de class  poque son palabras recervadas
//reack esta orientado a componentes
import Header from './Global/Headers';
import Content from'./Global/Content';
import Footer from './Global/Footer';
class App extends Component {
  render() {
    return ( 
      <div className="App">
      <Header/>
      <Content/>
      <Footer/>
      </div>
    );
  }
}

export default App;
